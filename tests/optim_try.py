import functools
import timeit
import numpy as np
from scipy.signal import correlate2d, convolve2d

def backward_conv2d_optim(x, dout, kernel):
    """
    A naive implementation of the backward pass for a convolutional layer.

    Inputs:
    - x: Layer input
    - dout: Upstream derivatives.
    - kernel: Weight kernel.   

    Returns a tuple of:
    - dx: Gradient with respect to x
    - dw: Gradient with respect to w
     """
    assert x.shape == dout.shape
    assert x.ndim == kernel.ndim
    # flipped_kernel = np.flipud(np.fliplr(kernel))
    # dx = correlate2d(dout, flipped_kernel, mode='same')
    dx = convolve2d(dout, kernel, mode='same')
    h, w = dout.shape
    k_h, k_w = kernel.shape
    dw = np.zeros_like(kernel)
    p = int(np.floor(np.divide(k_h, 2)))
    q = int(np.floor(np.divide(k_w, 2)))
    padded_x = np.pad(x, ((p, p), (q, q)), mode='constant')
    dw = correlate2d(padded_x, dout, 'valid')
    return dx, dw


def backward_conv2d(x, dout, kernel):
    """
    A naive implementation of the backward pass for a convolutional layer.

    Inputs:
    - x: Layer input
    - dout: Upstream derivatives.
    - kernel: Weight kernel.   

    Returns a tuple of:
    - dx: Gradient with respect to x
    - dw: Gradient with respect to w
     """
    assert x.shape == dout.shape
    assert x.ndim == kernel.ndim
    flipped_kernel = np.flipud(np.fliplr(kernel))
    dx = correlate2d(dout, flipped_kernel, mode='same')
    h, w = dout.shape
    k_h, k_w = kernel.shape
    dw = np.zeros_like(kernel)
    p = int(np.floor(np.divide(k_h, 2)))
    q = int(np.floor(np.divide(k_w, 2)))
    padded_x = np.pad(x, ((p, p), (q, q)), mode='constant')
    padded_dout = np.pad(dout, ((p, p), (q, q)), mode='constant')
    for i in range(k_h):
        for j in range(k_w):
            dw[i, j] = padded_x[i:h+i, j:w+j].reshape(-1, ).dot(padded_dout[i:h+i, j:w+j].reshape(-1, ))
    return dx, dw

if __name__ == "__main__":

    x = np.random.randn(32, 32)
    y = np.random.randn(32, 32)
    kernel = np.random.randn(3, 3)
    optim = functools.partial(backward_conv2d_optim, x, y, kernel)
    reg = functools.partial(backward_conv2d, x, y, kernel)
    number = 10000
    optim_res = timeit.timeit(optim, number=number)
    reg_res = timeit.timeit(reg, number=number)
    print(f'optim_res = {optim_res}, reg_res = {reg_res}')
    print(f'avg_optim_res = {optim_res/number}, reg_res = {reg_res/number}')
