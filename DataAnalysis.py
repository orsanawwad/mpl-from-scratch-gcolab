import collections

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

train_data = np.load('./bin_data/train.npy')
train_labels = train_data[:4000, 0]

train_labels_counter = collections.Counter(train_labels)

plt.bar(train_labels_counter.keys(), train_labels_counter.values())

plt.savefig('./DataAnalysis/first_4000_train_labels_barplot.png', bbox_inches='tight')
